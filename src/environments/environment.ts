// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBWdZNkUdcr-1ABK2cUc2_rcA2GON0Cz9o",
    authDomain: "tpionicmpwin.firebaseapp.com",
    databaseURL: "https://tpionicmpwin.firebaseio.com",
    projectId: "tpionicmpwin",
    storageBucket: "tpionicmpwin.appspot.com",
    messagingSenderId: "326726050289",
    appId: "1:326726050289:web:fcead0b4368df9442c6450",
    measurementId: "G-MQPTNYED46"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
