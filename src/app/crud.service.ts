import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore/firestore';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private firestore: AngularFirestore) { }

  createStudent(student){
    return this.firestore.collection('Students').add(student);
  }
  updateStudents(student, id:number){
    return this.firestore.doc('Students/' + id).update(student);
  }
  getAllStudents(){
    return this.firestore.collection('Students').snapshotChanges();
  }
  deleteStudent(student_id){
    this.firestore.doc('Students/' + student_id).delete();
  }
}
