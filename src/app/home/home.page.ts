import { Component } from '@angular/core';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
students: any = [];
public studentName:string;
public studentAddress:string;
public studentAge:number;

  constructor(private crudService: CrudService) {}

ngOnInit(){
}
getAllStudents() {
  this.crudService.getAllStudents().subscribe((result)=> {
    this.students = result.map(element =>{
      return {id: element.payload.doc.id,
        name : element.payload.doc.data()['name'],
        age : element.payload.doc.data()['age'],
        adress : element.payload.doc.data()['adress'],

        
    }});
  })
}
}
